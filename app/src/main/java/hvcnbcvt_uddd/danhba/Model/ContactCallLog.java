package hvcnbcvt_uddd.danhba.Model;

public class ContactCallLog {
    private String nameLog;
    private String phoneNumberLog;
    private String Log;

    public ContactCallLog() {
    }

    public ContactCallLog(String nameLog, String phoneNumberLog, String log) {
        this.nameLog = nameLog;
        this.phoneNumberLog = phoneNumberLog;
        Log = log;
    }

    public String getNameLog() {
        return nameLog;
    }

    public void setNameLog(String nameLog) {
        this.nameLog = nameLog;
    }

    public String getPhoneNumberLog() {
        return phoneNumberLog;
    }

    public void setPhoneNumberLog(String phoneNumberLog) {
        this.phoneNumberLog = phoneNumberLog;
    }

    public String getLog() {
        return Log;
    }

    public void setLog(String log) {
        Log = log;
    }
}
