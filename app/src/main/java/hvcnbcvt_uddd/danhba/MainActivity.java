package hvcnbcvt_uddd.danhba;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Call;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import hvcnbcvt_uddd.danhba.Model.ContactCallLog;
import hvcnbcvt_uddd.danhba.Model.ContactInfo;
import hvcnbcvt_uddd.danhba.View.AddContactActivity;
import hvcnbcvt_uddd.danhba.control.ContactAdapter;
import hvcnbcvt_uddd.danhba.control.ContactCallLogAdapter;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, View.OnClickListener {

    //tab1
    EditText edt_show;
    String phoneNumber = "";

    //tab2
    ListView lvLog;
    ContactCallLogAdapter adapter;
    ArrayList<ContactCallLog> list;
    public static final String[] PERMISSION_LIST = {
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_CALL_LOG
    };


    //tab3
    ArrayList<ContactInfo> mList;
    ListView lvContact;
    ContactAdapter mAdapter;
    SearchView searchView;
    ImageButton btn_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        //check quyen
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String permission : PERMISSION_LIST) {
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(PERMISSION_LIST, 0);
                    return;
                }
            }
        }
        controls();
        events();
    }

    private void events() {
        tab1();
        tab2();
        tab3();
    }
    //tab1
    private void tab1() {
        int[] idButton = {R.id.btn_0,R.id.btn_1,R.id.btn_2,R.id.btn_3,R.id.btn_4,R.id.btn_5,R.id.btn_6,
                R.id.btn_7,R.id.btn_8,R.id.btn_9,R.id.btn_call,R.id.btn_mess,R.id.btn_sao,R.id.btn_thang,R.id.btn_delete };
        for (int id:idButton)
        {
            View view = (View) findViewById(id);
            view.setOnClickListener(this);
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_call:
                call(phoneNumber);
                break;
            case R.id.btn_mess:
                mess(phoneNumber);
                break;
            case R.id.btn_delete:
                delete();
                break;
            default:
                phoneNumber += ((Button)view).getText().toString();
                edt_show.setText(phoneNumber);

        }
    }

    private void delete() {
        if (phoneNumber.length() == 0) {
            return;
        }
        int index = phoneNumber.length() - 1;
        phoneNumber = phoneNumber.substring(0,index);
        edt_show.setText(phoneNumber);
    }

    private void mess(String phone) {
        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("sms:"+phone));
        intent.putExtra("sms_body"," ");
        startActivity(intent);
    }

    private void call(String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //tab2
    private void tab2() {
        list = getCallLog();
        adapter = new ContactCallLogAdapter(this, list);
        adapter.notifyDataSetChanged();
        lvLog.setAdapter(adapter);

    }
    private ArrayList<ContactCallLog> getCallLog() {
        ArrayList<ContactCallLog> lsLog = new ArrayList<>();
        Cursor cursor = managedQuery(CallLog.Calls.CONTENT_URI,null,null,null,null);
        int IndexNumber = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int IndexType = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int IndexDuration = cursor.getColumnIndex(CallLog.Calls.DURATION);

        while (cursor.moveToNext()){
            String number = cursor.getString(IndexNumber);
            String type = cursor.getString(IndexType);
            String duration = cursor.getString(IndexDuration);
            String dir = null;
            int dircode = Integer.parseInt(type);
            switch (dircode){
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            ContactCallLog callLog = new ContactCallLog(dir,number,duration);
            lsLog.add(callLog);

        }

        return lsLog;
    }


    //tab3
    private void tab3() {
        setupData();
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                call(mList.get(i).getPhoneNumber().toString());
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddContactActivity.class);
                startActivity(intent);
            }
        });

    }
    private void setupData() {
        mList = getData();
        mAdapter = new ContactAdapter(this, mList);
        lvContact.setAdapter(mAdapter);
    }
    public ArrayList<ContactInfo> getData() {
        ArrayList<ContactInfo> data = new ArrayList<>();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();

        int indexID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
        int indexName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexPhone = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        int indexImage = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);

        while (!cursor.isAfterLast()) {
            String id = cursor.getString(indexID);
            String name = cursor.getString(indexName);
            String phone = cursor.getString(indexPhone);
            String photo = cursor.getString(indexImage);
            ContactInfo ci = new ContactInfo(id, name, phone, photo);
            data.add(ci);
            cursor.moveToNext();
        }
        System.out.println("SIZE " + data.size());
        return data;
    }
    // Tìm kiếm theo tên
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<ContactInfo> ds = new ArrayList<>();
        for (ContactInfo contactSearch : mList) {
            String name = contactSearch.getName().toLowerCase();
            if ((name.contains(newText))) {
                ds.add(contactSearch);
            }
        }
        mAdapter.setFilter(ds);
        return true;
    }


    private void controls() {
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tab1 = tabHost.newTabSpec("t1");
        tab1.setContent(R.id.tab1);
        tab1.setIndicator("Gọi điện");
        tabHost.addTab(tab1);

        TabHost.TabSpec tab2 = tabHost.newTabSpec("t2");
        tab2.setContent(R.id.tab2);
        tab2.setIndicator("Nhật ký");
        tabHost.addTab(tab2);

        TabHost.TabSpec tab3 = tabHost.newTabSpec("t3");
        tab3.setContent(R.id.tab3);
        tab3.setIndicator("Danh bạ");
        tabHost.addTab(tab3);
        //xử lý Tab1
        edt_show = (EditText) findViewById(R.id.edt_show);

        //Xử lý Tab2
        lvLog = (ListView) findViewById(R.id.lv_contact_log);

        //Xử lý Tab3
        lvContact = findViewById(R.id.lv_contact);
        btn_add = (ImageButton) findViewById(R.id.btn_add);
        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener((SearchView.OnQueryTextListener) this);
    }
}
