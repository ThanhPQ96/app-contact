package hvcnbcvt_uddd.danhba.control;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hvcnbcvt_uddd.danhba.Model.ContactCallLog;
import hvcnbcvt_uddd.danhba.R;

public class ContactCallLogAdapter extends ArrayAdapter<ContactCallLog>
{

    private ArrayList<ContactCallLog> list;
    private Context context;

    public ContactCallLogAdapter(@NonNull Context context, @NonNull ArrayList<ContactCallLog> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.list = objects;
        this.context = context;
    }



    @NonNull
    @Override
    public View getView(int position, @Nullable View v, @NonNull ViewGroup parent) {
        if (position >= list.size())
            return v;
        ViewHolder viewHolder;
        if(v == null)
        {
            viewHolder =  new ViewHolder();
            v = LayoutInflater.from(context).inflate(R.layout.tab2_item,parent,false);
            viewHolder.item2_img = v.findViewById(R.id.item2_img);
            viewHolder.item2_tv_name = v.findViewById(R.id.item2_tv_name);
            viewHolder.item2_tv_number = v.findViewById(R.id.item2_tv_number);
            viewHolder.item2_tv_log = v.findViewById(R.id.item2_tv_log);
            v.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) v.getTag();
        }

        ContactCallLog info = list.get(position);
        viewHolder.item2_tv_number.setText(info.getPhoneNumberLog());
        viewHolder.item2_tv_name.setText(info.getNameLog());
        viewHolder.item2_tv_log.setText(info.getLog());
        viewHolder.item2_img.setImageResource(R.mipmap.ic_launcher_round);
        return v;
    }

    private class ViewHolder {
        ImageView item2_img;
        TextView item2_tv_name;
        TextView item2_tv_number;
        TextView item2_tv_log;
    }
}
