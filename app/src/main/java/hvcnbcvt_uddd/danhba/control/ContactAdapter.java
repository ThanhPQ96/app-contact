package hvcnbcvt_uddd.danhba.control;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hvcnbcvt_uddd.danhba.Model.ContactInfo;
import hvcnbcvt_uddd.danhba.R;

public class ContactAdapter extends ArrayAdapter<ContactInfo> {

    private ArrayList<ContactInfo> mLists;
    private Context mCtx;

    public ContactAdapter(@NonNull Context context, @NonNull ArrayList<ContactInfo> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.mCtx = context;
        this.mLists = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View v, @NonNull ViewGroup parent) {
        if (position >= mLists.size())
            return v;
        ViewHolder viewHolder;
        if (v == null) {
            viewHolder = new ViewHolder();
            v = LayoutInflater.from(mCtx).inflate(R.layout.item_tab3, parent, false);

            viewHolder.imageView = v.findViewById(R.id.item_imageView);
            viewHolder.tvName = v.findViewById(R.id.item_tvName);
            viewHolder.tvPhoneNumber = v.findViewById(R.id.item_tvNumber);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }

        ContactInfo info = mLists.get(position);
        viewHolder.tvName.setText(info.getName());
        viewHolder.tvPhoneNumber.setText(info.getPhoneNumber());
        try {
            viewHolder.imageView.setImageURI(Uri.parse(info.getPhotoUri()));
        } catch (Exception e) {
            viewHolder.imageView.setImageResource(R.mipmap.ic_launcher_round);
        }
        return v;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView tvName;
        TextView tvPhoneNumber;
    }

    public void setFilter(ArrayList<ContactInfo> ds){
        mLists=new ArrayList<>();
        mLists.addAll(ds);
        notifyDataSetChanged();
    }
}
